namespace beHiredApiBase.Domain.Models.Account
{
    public class Nationality
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string CountryInitial { get; set; }
    }
}