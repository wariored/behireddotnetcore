namespace beHiredApiBase.Domain.Models.BaseModels
{
    public class UserType
    {
        /// <summary>
        /// The type Id
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// The type string value
        /// </summary>
        public  string TypeName { get; set; }
    }
}