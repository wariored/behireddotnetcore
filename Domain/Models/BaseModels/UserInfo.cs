using System;
using System.Security.Policy;
using beHiredApiBase.Domain.Models.Account;

namespace beHiredApiBase.Domain.Models.BaseModels
{
    public class UserInfo
    {
        public int Id { get; set; }
        
        public int UserTypeId { get; set; }

        public UserType UserType { get; set; }
        
        public int UserId { get; set; }

        public User User { get; set; }

        public string Name { get; set; }

        public string OldName { get; set; }

        public bool VisibleWhenOnline { get; set; }

        public string Introduction { get; set; }

        public int HourlyRate { get; set; }

        public string TagLine { get; set; }

        public string Skills { get; set; }

        public Url AvatarUrl { get; set; }

        public Url CoverLetterUrl { get; set; }

        public int CurrentMonthProfileViews { get; set; }

        public int CurrentYearProfileViews { get; set; }

        public int TotalProfileViews { get; set; }

        public bool TwoStepMailVerification { get; set; }

        public DateTime NameChangedAt { get; set; }

        public Nationality Nationality { get; set; }

        public DateTime CreatetAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}