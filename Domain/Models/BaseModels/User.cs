using System;
using System.Collections.Generic;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace beHiredApiBase.Domain.Models.BaseModels
{
    public class User
    {
        public User()
        {
        }

        public int Id { get; set; }

        public string Email { get; set; }

        [JsonIgnore] public string Password { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime LastLogin { get; set; }

        public IEnumerable<UserType> UserTypes { get; set; }

        public string Token { get; set; }
    }
}