using System;
using System.Reflection;
using System.Resources;
using beHiredApiBase.Infrastructure.Messages;


namespace beHiredApiBase.Domain.Responses
{
    public class SimpleResponse
    {

        public SimpleResponse(int code, string message)
        {
            if (code == default(int) || message == string.Empty)
            {
                throw new Exception("Response error");
            }
            Code = code;
            Message = message;
        }

        /// <summary>
        /// the response code
        /// </summary>
        private int Code { get; }

        /// <summary>
        /// the response message
        /// </summary>
        private string Message { get; }
    }
}