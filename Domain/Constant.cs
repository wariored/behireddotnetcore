using System.Collections.Generic;

namespace beHiredApiBase.Domain
{
    /// <summary>
    /// all constant variables in Domain
    /// <see cref="beHiredApiBase.Domain.Constant"/>
    /// </summary>
    public static class Constant
    {
        /// <summary>
        /// The connection string name
        /// </summary>
        public static readonly string ConnectionStringName = "DefaultConnection";

        /// <summary>
        /// stocked procedure that insert user
        /// </summary>
        public static readonly string InsertUserSockedProcedure = "bhd_i_users_stocked_procedure";

        /// <summary>
        /// stocked procedure that update user
        /// </summary>
        public static readonly string UpdateUserByIdSockedProcedure = "bhd_u_users_stocked_procedure";

        /// <summary>
        /// existing type of user
        /// </summary>
        public static readonly List<string> UserTypeNames = new List<string> {"COMPANY", "FREELANCER", "INSIDE", "EXTEND"};
    }
}