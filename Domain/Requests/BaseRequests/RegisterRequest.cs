using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace beHiredApiBase.Domain.Requests.BaseRequests
{
    public class  RegisterRequest
    {
        public RegisterRequest()
        {
            CreatedAt = DateTime.Now;
        }

        public string Email { get; set; }
        public string Password { get; set; }

        public List<string> UserTypeNames { get; set; }
        public DateTime CreatedAt { get; }
    }
}