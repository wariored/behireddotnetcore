namespace beHiredApiBase.Domain.Requests.BaseRequests
{
    public class LoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}