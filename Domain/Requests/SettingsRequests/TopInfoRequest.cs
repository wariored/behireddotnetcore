namespace beHiredApiBase.Domain.Requests.SettingsRequests
{
    public class TopInfoRequest
    {
        public string Name { get; set; }
        
        public string Email { get; set; }
    }
}