namespace beHiredApiBase.Domain.Requests.SettingsRequests
{
    public class SetPasswordAndSecurityRequest
    {
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

        public bool TwoStepMailVerification { get; set; }
    }
}