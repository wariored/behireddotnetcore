using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using beHiredApiBase.Domain.Models.BaseModels;
using beHiredApiBase.Domain.Requests.BaseRequests;

namespace beHiredApiBase.Domain.Interfaces.BaseProviders
{
    /// <summary>
    /// Interface to provide users data
    /// </summary>
    public interface IUserDataProvider
    {
        /// <summary>
        /// Insert user into database
        /// </summary>
        /// <param name="request"></param>
        /// <returns>bool</returns>
        Task<bool> InsertUserAsync(RegisterRequest request);
        
        /// <summary>
        /// Update user into database
        /// </summary>
        /// <param name="user"></param>
        /// <returns>bool</returns>
        Task<bool> UpdateUserAsync(User user);

        /// <summary>
        /// get user from database
        /// </summary>
        /// <param name="email"></param>
        /// <returns>User: can be null</returns>
        Task<User> GetUserAsync(string email);
        
        /// <summary>
        /// get user from database
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>User: can be null</returns>
        Task<User> GetUserAsync(int userId);

        /// <summary>
        /// check if user already exists
        /// </summary>
        /// <param name="email"></param>
        /// <returns>bool</returns>
        Task<bool> UserExistsAsync(string email);

        /// <summary>
        /// fetch the existing UserType from the DB
        /// </summary>
        /// <param name="userTypeNames"></param>
        /// <returns>UserType</returns>
        Task<IEnumerable<UserType>> GetUserTypesListAsync(List<string> userTypeNames);
        
        /// <summary>
        /// Get the userInfo from userId and userTypeId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userTypeId"></param>
        /// <returns>UserInfo</returns>
        Task<UserInfo> GetUserInfoAsync(int userId, int userTypeId);
        
        //Task<UserType> GetSingleUserType(string userTypeName);
        
        
    }
}