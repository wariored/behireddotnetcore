using System;
using System.Threading.Tasks;
using beHiredApiBase.Domain;
using beHiredApiBase.Domain.Interfaces.AccountProviders;
using Microsoft.Extensions.Configuration;

namespace beHiredApiBase.Infrastructure.Providers.AccountProviders
{
    public class SettingsDataProvider : ISettingsDataProvider
    {
        /// <summary>
        /// The sql connection string.
        /// </summary>
        private readonly string _sqlConnectionString;

        public SettingsDataProvider(IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            _sqlConnectionString = configuration.GetConnectionString(Constant.ConnectionStringName);
        }
        
        //public Task<>
    }
}