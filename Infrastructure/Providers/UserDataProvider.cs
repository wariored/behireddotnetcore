using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using beHiredApiBase.Domain.Interfaces.BaseProviders;
using beHiredApiBase.Domain.Models.BaseModels;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using beHiredApiBase.Domain;
using beHiredApiBase.Domain.Requests.BaseRequests;
using Microsoft.Extensions.Configuration;
using Dapper;

namespace beHiredApiBase.Infrastructure.Providers
{
    /// <summary>
    /// User SQL Repository
    /// </summary>
    public class UserDataProvider : IUserDataProvider
    {
        /// <summary>
        /// The sql connection string.
        /// </summary>
        private readonly string _sqlConnectionString;

        public UserDataProvider(IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            _sqlConnectionString = configuration.GetConnectionString(Constant.ConnectionStringName);
        }

        public async Task<bool> InsertUserAsync(RegisterRequest req)
        {
            if (req == null)
            {
                return false;
            }

            IEnumerable<UserType> userTypesList = await GetUserTypesListAsync(req.UserTypeNames);
            using (IDbConnection sqlConnection = new SqlConnection(_sqlConnectionString))
            {
                DynamicParameters parameter = new DynamicParameters();
                parameter.Add("@Email", req.Email);
                parameter.Add("@Password", req.Password);
                parameter.Add("@createdAt", req.CreatedAt);

                var sql = "INSERT INTO USERS (Email, Password, CreatedAt) values (@Email, @Password, @CreatedAt)";

                foreach (var userType in userTypesList)
                {
                    sql +=
                        "INSERT INTO USERS_USER_TYPES(UserId, UserTypeId) VALUES((SELECT Id FROM USERS WHERE EMAIL=@EMAIL)," +
                        userType.Id + ");";
                }

                var result = await sqlConnection.ExecuteAsync(sql, parameter);
                return result > 0;
            }
        }

        public async Task<bool> UpdateUserAsync(User user)
        {
            return false;
        }

        public async Task<User> GetUserAsync(string email)
        {
            using (IDbConnection sqlConnection = new SqlConnection(_sqlConnectionString))
            {
                var sql = "select * FROM USERS WHERE Email=@email";

                var user = await sqlConnection.QuerySingleOrDefaultAsync<User>(sql, new {email});
                if (user != null)
                {
                    user.UserTypes = await GetUserTypesForUserAsync(user.Id, sqlConnection);
                }

                return user;
            }
        }
        
        public async Task<User> GetUserAsync(int userId)
        {
            using (IDbConnection sqlConnection = new SqlConnection(_sqlConnectionString))
            {
                var sql = "select * FROM USERS WHERE Id=@userId";

                var user = await sqlConnection.QuerySingleOrDefaultAsync<User>(sql, new {userId});
                if (user != null)
                {
                    user.UserTypes = await GetUserTypesForUserAsync(user.Id, sqlConnection);
                }

                return user;
            }
        }

        public async Task<bool> UserExistsAsync(string email)
        {
            User user = await GetUserAsync(email);

            return user != null;
        }
        
        /// <summary>
        /// Get the userTypes for a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sqlConnection"></param>
        /// <returns>IEnumerable of UserType</returns>
        private async Task<IEnumerable<UserType>> GetUserTypesForUserAsync(int userId, IDbConnection sqlConnection)
        {
            var sql = "select ut.Id, ut.Type TypeName FROM USER_TYPES ut INNER JOIN USERS_USER_TYPES uut " +
                      "ON ut.Id = uut.UserTypeId WHERE uut.UserId=@userId";
            var userTypesForUser = await sqlConnection.QueryAsync<UserType>(sql, new {userId});
            var typesForUserList = userTypesForUser.ToList();
            if (!typesForUserList.Any()) throw new RowNotInTableException("User has no UserType in the DB");
            return typesForUserList;
        }

        public async Task<IEnumerable<UserType>> GetUserTypesListAsync(List<string> userTypeNames)
        {
            using (IDbConnection sqlConnection = new SqlConnection(_sqlConnectionString))
            {
                var sql = "select Id, Type TypeName FROM USER_TYPES WHERE TYPE in @TYPES";

                var userTypes = await sqlConnection.QueryAsync<UserType>(sql, new {TYPES = userTypeNames});

                var userTypesList = userTypes.ToList();
                if (!userTypesList.Any()) throw new RowNotInTableException("There is no UserType in the DB");
                return userTypesList;
            }
        }

        public Task<UserInfo> GetUserInfoAsync(int userId, int userTypeId)
        {
            
            throw new NotImplementedException();
        }
        
    }
}