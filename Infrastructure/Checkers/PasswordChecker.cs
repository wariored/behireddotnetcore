using System.Web.Helpers;

namespace beHiredApiBase.Infrastructure.Checkers
{
    public static class PasswordChecker
    {
        public static string HashPassword(string password)
        {
            return Crypto.HashPassword(password);
        }

        public static bool VerifyHashedPassword(string hashedPassword, string password)
        {
            return Crypto.VerifyHashedPassword(hashedPassword, password);
        }
    }
}