using beHiredApiBase.Domain.Requests.SettingsRequests;

namespace beHiredApiBase.Infrastructure.Checkers
{
    public static class AccountChecker
    {
        public static bool isValid(this SetPasswordAndSecurityRequest req)
        {

            return req.NewPassword != req.OldPassword && req.NewPassword.Length >= 7;
        }
    }
}