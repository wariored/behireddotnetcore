using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using beHiredApiBase.Domain;
using beHiredApiBase.Domain.Requests.BaseRequests;
using Microsoft.AspNetCore.Http;

namespace beHiredApiBase.Infrastructure.Checkers
{
    public static class AuthChecker
    {
        /// <summary>
        /// check if the registration request is a valid one
        /// </summary>
        /// <param name="request"></param>
        /// <returns>bool</returns>
        public static bool isValid(this RegisterRequest request)
        {
            if (request.Email == null || request.Password == null || request.UserTypeNames == null) return false;
            bool isTypesCorrect = request.UserTypeNames.Distinct().Count() == request.UserTypeNames.Count;
            if (isTypesCorrect)
            {
                foreach (var typeName in request.UserTypeNames)
                {
                    if (Constant.UserTypeNames.Contains(typeName.ToUpper())) continue;
                    isTypesCorrect = false;
                    break;
                }
            }

            return isTypesCorrect && isEmailValid(request.Email) && request.Password.Length >= 7;
        }

        public static bool isValid(this LoginRequest request)
        {
            return isEmailValid(request.Email) && request.Password.Length > 7;
        }

        public static bool isEmailValid(string email)
        {
            var attr = new EmailAddressAttribute();
            return attr.IsValid(email);
        }

        /// <summary>
        /// Get the user's email from the token claim
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns>string</returns>
        /// <exception cref="Exception"></exception>
        public static string GetUserEmailFromRequestToken(HttpContext httpContext)
        {
            var email = string.Empty;
            if (httpContext.User.Identity is ClaimsIdentity identity)
            {
                email = identity.FindFirst(ClaimTypes.Name).Value;
            }

            if (email == null) throw new Exception("JWT claim not found");

            return email;
        }
    }
}