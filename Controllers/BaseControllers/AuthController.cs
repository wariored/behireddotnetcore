using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using beHiredApiBase.Domain.Interfaces.BaseProviders;
using beHiredApiBase.Domain.Models.BaseModels;
using beHiredApiBase.Domain.Requests.BaseRequests;
using beHiredApiBase.Infrastructure.Checkers;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace beHiredApiBase.Controllers.BaseControllers
{
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IUserDataProvider _userDataProvider;
        private readonly string _secretKey;

        public AuthController(IConfiguration configuration, IUserDataProvider userDataProvider)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            _secretKey = configuration["SecretKey"];
            _userDataProvider = userDataProvider;
        }

        [HttpPost("/authentication/register")]
        public async Task<IActionResult> RegisterActionAsync(RegisterRequest request)
        {
            if (!request.isValid())
            {
                return BadRequest(new {message = "not a valid request"});
            }

            // check if user already exist in DB
            var userExists = await _userDataProvider.UserExistsAsync(request.Email);
            if (userExists)
            {
                //var response = new SimpleResponse(400, "userAlreadyExists");
                return BadRequest(new {message = "user already exists"});
            }

            request.Password = PasswordChecker.HashPassword(request.Password);
            var isInserted = await _userDataProvider.InsertUserAsync(request);
            if (isInserted)
            {
                User user = await _userDataProvider.GetUserAsync(request.Email);
                user.Token = GenerateToken(user.Email);
                return Ok(user);
            }


            return StatusCode(500);
        }


        [HttpPost("/authentication/login")]
        public async Task<IActionResult> LoginActionAsync(LoginRequest request)
        {
            if (!request.isValid()) return BadRequest(new {message = "not a valid request"});
            
            // check if user already exist in DB
            var userExists = await _userDataProvider.UserExistsAsync(request.Email);
            if (!userExists) return BadRequest(new {message = "user does not exist"});
            
            User user = await _userDataProvider.GetUserAsync(request.Email);
            
            if (!PasswordChecker.VerifyHashedPassword(user.Password, request.Password))
                return BadRequest(new {message = "password is not correct"});
            
            user.Token = GenerateToken(user.Email);
            
            return Ok(user);
        }

        [Authorize]
        [HttpGet("test")]
        public IActionResult Test()
        {
            var email = AuthChecker.GetUserEmailFromRequestToken(HttpContext);

            return Ok(email);
        }

        /// <summary>
        /// Generate token for user
        /// </summary>
        /// <param name="email"></param>
        /// <returns>string</returns>
        private string GenerateToken(string email)
        {
            var claims = new Claim[]
            {
                new Claim(ClaimTypes.Name, email),
                new Claim(JwtRegisteredClaimNames.Nbf, new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp,
                    new DateTimeOffset(DateTime.Now.AddDays(7)).ToUnixTimeSeconds().ToString())
            };

            var token = new JwtSecurityToken(
                new JwtHeader(new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_secretKey)),
                    SecurityAlgorithms.HmacSha256)),
                new JwtPayload(claims));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}